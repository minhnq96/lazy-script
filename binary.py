#!/usr/bin/env python3

import sys
import argparse
import urllib.parse

from colorama import *

##### Colors to be use
def G(string): return Fore.GREEN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL 
def g(string): return Fore.GREEN + string + Fore.RESET
def B(string): return Fore.BLUE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def b(string): return Fore.BLUE + string + Fore.RESET
def R(string): return Fore.RED + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def r(string): return Fore.RED + string + Fore.RESET
def Y(string): return Fore.YELLOW + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def y(string): return Fore.YELLOW + string + Fore.RESET
def M(string): return Fore.MAGENTA + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def m(string): return Fore.MAGENTA + string + Fore.RESET
def C(string): return Fore.CYAN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def c(string): return Fore.CYAN + string + Fore.RESET
def W(string): return Fore.WHITE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def w(string): return Fore.WHITE + string + Fore.RESET 

def _G(string): return Back.GREEN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _g(string): return Back.GREEN + string + Back.RESET
def _B(string): return Back.BLUE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _b(string): return Back.BLUE + string + Back.RESET
def _R(string): return Back.RED + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _r(string): return Back.RED + string + Back.RESET
def _G(string): return Back.GELLOW + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _y(string): return Back.GELLOW + string + Back.RESET
def _M(string): return Back.MAGENTA + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _m(string): return Back.MAGENTA + string + Back.RESET
def _C(string): return Back.CYAN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _c(string): return Back.CYAN + string + Back.RESET
def _W(string): return Back.WHITE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _w(string): return Back.WHITE + string + Back.RESET

##### Argument for script
parser = argparse.ArgumentParser(
	description = "Binary Encode-Decode Script",
	epilog = "Author : Minhg Nguyen Quang"
	)

parser.add_argument("-e", "--encode", metavar='"string"', help='Binary Encode a string')
parser.add_argument("-d", "--decode", metavar='"string"', help='Binary Decode a string')

args = parser.parse_args()

##### Encode function
def encode():
	string_encode = f"{args.encode}"
	e = ' '.join('{0:08b}'.format(ord(x), 'b') for x in string_encode)
	print(G("\n[+] ") + B("The given string : ") + R(string_encode))
	print(G("[+] ") + B("Binary Encode : ") + R(e) + "\n")

def decode():
	string_decode = f"{args.decode}"
	d = "".join([chr(int(binary, 2)) for binary in string_decode.split(" ")])
	print(G("\n[+] ") + B("The given string : ") + R(string_decode))
	print(G("[+] ") + B("Binary Decode : ") + R(d) + "\n")

def main():
	string_encode = f"{args.encode}"
	string_decode = f"{args.decode}"

	if string_encode != "None" and string_decode == "None":
		encode()
	elif string_encode == "None" and string_decode != "None":
		decode()
	else:
		print(G("\n[!] ") + R("I don't know what to do\n"))

if __name__ == '__main__':
	main()
