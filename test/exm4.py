#!/usr/bin/env python3

import re

def to_binary(text):
    return [f"{ord(char):08b}" for char in text]

def to_text(binary):
    return ''.join(chr(int(b, 2)) for b in binary)

def chunk_string(s, length):
    return [s[i:i+length] for i in range(0, len(s), length)]

def xor(str_binary, key):
    key = str(key)
    output = []
    
    extended_key = (key * ((len(str_binary[0]) // len(key)) + 1))[:len(str_binary[0])]

    for i in range(len(str_binary)):
        xor_result = ''.join(
            str(int(str_binary[i][j]) ^ int(extended_key[j])) for j in range(len(str_binary[i]))
        )
        output.append(xor_result)

    return output

def encode(message, key):
    binary_message = to_binary(message)
    encoded_binary = xor(binary_message, key)
    return encoded_binary

# Hàm giải mã
def decode(cipher, key):
    decoded_binary = xor(cipher, key)
    return to_text(decoded_binary)

encoded_data = ["22252325" ,"22252325" ,"23243334" ,"23242325" ,"23242335" ,"22252225" ,"23242324" ,"22252325" ,"22252325" ,"23242235" ,"22252325" ,"23242224" ,"23243335" ,"23242224" ,"22252325" ,"22252235" ,"23242234" ,"22252225" ,"23242225" ,"22252324" ,"23242225" ,"22252325" ,"23243335" ,"22252235" ,"22252324" ,"22252325" ,"23242224" ,"22252235" ,"23242325" ,"22252324" ,"22252234" ,"23242324" ,"23353325"]

key_list = [str(i).zfill(4) for i in range(10000)]
for a in key_list:
    try:
        c = decode(encoded_data, a)
        b = re.findall(r'([a-fA-F\d]{32})', c)
        if re.findall(r"([a-fA-F\d]{32})", c):
            print(f'Key: {a}')
            print(f"Flag: {b}")
    except ValueError:
        pass
