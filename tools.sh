#!/bin/bash

# Install most of the usefull tools that i use for new VM-Machine

sudo apt-get install -y gcc perl make \
	python3 python3-dev python3-venv python3-pip -y \
	npm gem nodejs git -y \
	seclists terminator -y \
	ffuf wfuzz -y \
	steghide binwalk -y \
	imagemagick eog nautilus -y \
	sublist3r -y \
	
