#!/usr/bin/env python3

import sys
# For rot13 encryption
import codecs
# For argument parsing
import argparse

parser = argparse.ArgumentParser(
	description="ro13 decoder",
	epilog="Author : Minhg Nguyen Quang"
	)

parser.add_argument("-d", "--decode", metavar="<string>")

args = parser.parse_args()

encoded = args.decode

print(codecs.encode(encoded, 'rot13'))
