#!/bin/bash

## Color code variables
red="\e[0;91m"
yellow="\e[0;33m"
blue="\e[0;94m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
green="\e[0;92m"
white="\e[0;97m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"

date=$(date)

## Header
echo -e "${blue}${bold}============================== My assist notes ==============================${reset}"
echo -e "${yellow}${date}${green}"

###############################################################################

## Nmap initial
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running initial port scan with NMAP${reset}"

echo -e "## Default script and detect version/service"
echo -e "${red}nmap -sC -sV \$IP${reset}"

echo -e '## Default script and detect version/service with save output to a normal file'
echo -e "${red}nmap -sC -sV -oN inital_scan \$IP${reset}"

echo -e '## Default script and detect version/service with more verbose and fast timing and performance'
echo -e "${red}nmap -sC -sV -vvv -T4 \$IP${reset}"

echo -e '## Default script and detect version/service with more verbose with fast timing and performance also with save output to a normal file'
echo -e "${red}nmap -sC -sV -vvv -T4 -oN inital_scan \$IP${reset}"

###############################################################################

## Nmap a specific port
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running a specific port scan with NMAP${reset}"

echo -e '## Detect verison/service on specific port(s) with NMAP'
echo -e "${red}nmap -p \$PORT -sV \$IP${reset}"

###############################################################################

## Nmap all port and Rustscan
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running all port scan with NMAP or/and Rustscan${reset}"

## NMAP command
echo -e "${green}${bold}\n[++] Running all port scan with NMAP${reset}"

echo -e '## Scan for all port and detect version/service on open ports only'
echo -e "${red}nmap -p- -sV \$IP --open${reset}"

echo -e '## Scan for all port and detect version/service on open ports only with more verbose and fast timing and performance'
echo -e "${red}nmap -p- -sV -vvv -T4 \$IP --open${reset}"

echo -e '## Scan for all port and detect version/service on open ports only with more verbose and fast timing and performance also with save output to a normal file'
echo -e "${red}nmap -p- -sV -vvv -T4 -oN all_port_scan \$IP --open${reset}"

## Rustscan command
echo -e "${green}${bold}\n[++] Running all port scan with Rustscan${reset}"

echo -e '## Default rustscan '
echo -e "${red}rustscan -a \$IP${reset}"

echo -e '## Default rustscan with save output to a normal file'
echo -e "${red}rustscan -a \$IP -- -oN inital_rustscan${reset}"

###############################################################################

## Banner grabbing with nc and/or telnet
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Banner grabbing${reset}"

echo -e '## Banner grabbing with NC and/or TELNET'
echo -e "${red}nc -v \$IP \$PORT${reset}"
echo -e "${red}telnet \$IP \$PORT${reset}"

###############################################################################

## Wfuzz for website files or directories
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running WFUZZ on target URL for files or directories${reset}"

echo -e '## WFUZZ for files on the URL'
echo -e "${red}wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-small-words.txt --hc 403,404 "http://\$IP/FUZZ"${reset}"

echo -e '## WFUZZ for directories on the URL'
echo -e "${red}wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-small-words.txt --hc 403,404 "http://\$IP/FUZZ/"${reset}"

###############################################################################

## Running WPSCAN
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running WPScan${reset}"

echo -e '## Defaul WPScan'
echo -e "${red}wpscan --url http://\$IP/\$wordpress_directory_go_here${reset}"

echo -e '## Enumerate WPScan on Vulnerable plugins - Vulnerable themes - Config backups - User IDs range'
echo -e "${red}wpscan --url http://\$IP/\$wordpress_directory_go_here -e vp,vt,u,cb${reset}"

echo -e '## Brute Force attack with WPScan'
echo -e "${red}wpscan --url http://\$IP/\$wordpress_directory_go_here -U \$USER_LIST -P \$PASS_LIST${reset}"

###############################################################################

## Running Hydra
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running Hydra${reset}"

echo -e '## Brute Force SNMP with Hydra'
echo -e "${red}hydra -P password-file.txt -f \$IP snmp${reset}"

echo -e '## Brute Force SMTP with Hydra'
echo -e "${red}hydra -P password-file.txt -f \$IP smtp${reset}"

echo -e '## Brute Force FTP with Hydra'
echo -e "${red}hydra -L user.txt -P /usr/share/wordlists/rockyou.txt -f \$IP ftp${reset}"

echo -e '## Brute Force SSH with Hydra'
echo -e "${red}hydra -L user.txt -P /usr/share/wordlists/rockyou.txt -f \$IP ssh${reset}"

echo -e '## Brute Force SMB with Hydra'
echo -e "${red}hydra -L user.txt -P /usr/share/wordlists/rockyou.txt -f \$IP smb${reset}"

echo -e '## Brute Force MYSQL with Hydra'
echo -e "${red}hydra -L user.txt -P /usr/share/wordlists/rockyou.txt -f \$IP mysql${reset}"

echo -e '## Brute Force RDP with Hydra'
echo -e "${red}hydra -L user.txt -P /usr/share/wordlists/rockyou.txt -f rdp://\$IP${reset}"

###############################################################################

## Running Hashcat or John
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running John or Hashcat${reset}"

### John
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[++] Running John${reset}"
echo -e "${red}john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt > plain_text.txt${reset}"

### Hashcat
echo -e "${blue}${bold}\n============================================================${reset}"
echo -e "${green}${bold}[+] Running Hashcat${reset}"
echo -e "${red}hashcat -a 0 -m \$MODE hash.txt /usr/share/wordlists/rockyou.txt -o plain_text.txt\n${reset}"
