#!/usr/bin/env python3

import sys
import argparse

from colorama import *

##### Colors to be use
def G(string): return Fore.GREEN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL 
def g(string): return Fore.GREEN + string + Fore.RESET
def B(string): return Fore.BLUE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def b(string): return Fore.BLUE + string + Fore.RESET
def R(string): return Fore.RED + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def r(string): return Fore.RED + string + Fore.RESET
def Y(string): return Fore.YELLOW + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def y(string): return Fore.YELLOW + string + Fore.RESET
def M(string): return Fore.MAGENTA + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def m(string): return Fore.MAGENTA + string + Fore.RESET
def C(string): return Fore.CYAN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def c(string): return Fore.CYAN + string + Fore.RESET
def W(string): return Fore.WHITE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def w(string): return Fore.WHITE + string + Fore.RESET 

def _G(string): return Back.GREEN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _g(string): return Back.GREEN + string + Back.RESET
def _B(string): return Back.BLUE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _b(string): return Back.BLUE + string + Back.RESET
def _R(string): return Back.RED + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _r(string): return Back.RED + string + Back.RESET
def _G(string): return Back.GELLOW + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _y(string): return Back.GELLOW + string + Back.RESET
def _M(string): return Back.MAGENTA + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _m(string): return Back.MAGENTA + string + Back.RESET
def _C(string): return Back.CYAN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _c(string): return Back.CYAN + string + Back.RESET
def _W(string): return Back.WHITE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _w(string): return Back.WHITE + string + Back.RESET 

##### Argument for script
parser = argparse.ArgumentParser(
	description="Rail Fence Cipher tool",
	epilog="Author : Minhg Nguyen Quang"
	)

parser.add_argument("-e", "--encrypt", metavar='"string"', help='Encrypt a string')
parser.add_argument("-d", "--decrypt", metavar='"string"', help='Decrypt a string')
parser.add_argument("-r", "--rail", metavar='"N"', type=int, help='Number of Rails')

args = parser.parse_args()

## Encrypt function
def encrypt(s,n):

	e = f"{args.encrypt}"
	d = f"{args.decrypt}"
	r = int(f"{args.rail}")

	fence = [[] for i in range(n)]
	rail  = 0
	var   = 1

	for char in s:
		fence[rail].append(char)
		rail += var

		if rail == n-1 or rail == 0:
			var = -var

	res = ''
	for i in fence:
		for j in i:
			res += j

	print(G("\n[+] ") + B("The given string : ") + R(e))
	print(G("[+] ") + B("Encoded to : ") + R(res) + "\n")

## Decrypt function
def decrypt(s,n):

	e = f"{args.encrypt}"
	d = f"{args.decrypt}"
	r = int(f"{args.rail}")

	fence = [[] for i in range(n)]
	rail  = 0
	var   = 1
	
	for char in s:
		fence[rail].append(char)
		rail += var

		if rail == n-1 or rail == 0:
			var = -var

	rFence = [[] for i in range(n)]

	i = 0
	l = len(s)
	s = list(s)
	for r in fence:
		for j in range(len(r)):
			rFence[i].append(s[0])
			s.remove(s[0])
		i += 1

	rail = 0
	var  = 1
	r = ''
	for i in range(l):
		r += rFence[rail][0]
		rFence[rail].remove(rFence[rail][0])
		rail += var

		if rail == n-1 or rail == 0:
			var = -var
	
	print(G("\n[+] ") + B("The given string : ") + R(d))
	print(G("[+] ") + B("Decoded to : ") + R(r) + "\n")

## Main function
def main():
	e = f"{args.encrypt}"
	d = f"{args.decrypt}"
	r = int(f"{args.rail}")

	if e != "None" and d == "None":
		encrypt(e, r)
	elif e == "None" and d != "None":
		decrypt(d, r)
	else:
		print(G("\n[!] ") + R("I don't know what to do"))

if __name__ == '__main__':
	main()
