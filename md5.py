#!/usr/bin/env python3

import sys
import time
import signal
import hashlib
import argparse

from colorama import *

##### Colors to be use
def G(string): return Fore.GREEN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL 
def g(string): return Fore.GREEN + string + Fore.RESET
def B(string): return Fore.BLUE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def b(string): return Fore.BLUE + string + Fore.RESET
def R(string): return Fore.RED + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def r(string): return Fore.RED + string + Fore.RESET
def Y(string): return Fore.YELLOW + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def y(string): return Fore.YELLOW + string + Fore.RESET
def M(string): return Fore.MAGENTA + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def m(string): return Fore.MAGENTA + string + Fore.RESET
def C(string): return Fore.CYAN + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def c(string): return Fore.CYAN + string + Fore.RESET
def W(string): return Fore.WHITE + Style.BRIGHT + string + Fore.RESET + Style.NORMAL
def w(string): return Fore.WHITE + string + Fore.RESET 

def _G(string): return Back.GREEN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _g(string): return Back.GREEN + string + Back.RESET
def _B(string): return Back.BLUE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _b(string): return Back.BLUE + string + Back.RESET
def _R(string): return Back.RED + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _r(string): return Back.RED + string + Back.RESET
def _G(string): return Back.GELLOW + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _y(string): return Back.GELLOW + string + Back.RESET
def _M(string): return Back.MAGENTA + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _m(string): return Back.MAGENTA + string + Back.RESET
def _C(string): return Back.CYAN + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _c(string): return Back.CYAN + string + Back.RESET
def _W(string): return Back.WHITE + Style.BRIGHT + string + Back.RESET + Style.NORMAL
def _w(string): return Back.WHITE + string + Back.RESET 

##### Argument for script
parser = argparse.ArgumentParser(
	description="md5 tools",
	epilog="Author : Minhg Nguyen Quang"
	)

parser.add_argument("-e", "--hash", metavar="<string>", help='To hash a string using md5')
parser.add_argument("-f", "--bruteforce", metavar="<string>", help='Brute force a md5 string')
parser.add_argument("-w", "--wordlist", metavar="<string>", help='Wordlist used to brute force the string. Default using /usr/share/wordlists/rockyou.txt')
parser.add_argument("-F", "--file", metavar="<string>", help='File to bruteforce or to hash')
parser.add_argument("-o", "--output", metavar="<string>", help='Output result into a file')

args = parser.parse_args()

##### To escape crtl+c gracefully
def error(string):
	preface = "\n[!] "
	print(R(preface) + string)
	sys.exit(1)

##### Hash function
def hash():
	string = f"{args.hash}"
	md5 = hashlib.md5(string.encode('latin-1'))
	print(G("\n[!]") + C(" The corresponding hash for ") + R(string) + C(" is: ") + R(md5.hexdigest()) + "\n")

##### MD5 a file
def hash_file():
	if f"{args.output}" == "None":
		print(G("\n[!]") + C(" The input file is: ") + R(f"{args.file}\n"))

		wl = open(f"{args.file}", "r", encoding='latin-1')
		for line in wl:
			md5 = hashlib.md5(line.encode('latin-1'))
			print(md5.hexdigest())
	else:
		print(G("\n[!]") + C(" The input file is: ") + R(f"{args.file}"))

		wl = open(f"{args.file}", "r", encoding='latin-1')
		for line in wl:
			f = open(f"{args.output}", "a")
			md5 = hashlib.md5(line.encode('latin-1'))
			f.write(f"{md5.hexdigest()}\n")
		print(G("\n[!]") + C(" The output file is: ") + R(f"{args.output}"))

##### BruteForce function
def bf():
	# The wordlist to use for check
	# hashed = input(G("\n[+] ") + B("Please give me a hash to work with: "))
	hashed = f"{args.bruteforce}"
	wordlist = f"{args.wordlist}"
	if wordlist == "None":
		wl = open("/usr/share/wordlists/rockyou.txt", "r", encoding='latin-1')
		for pwd in wl:
			a = pwd.encode()
			b = a[:-1]
			md5 = hashlib.md5(b).hexdigest()
			if hashed == md5:
				print(G("\n[!] ") + C("Cracked the given hash: ") + R(hashed) + G("\n[!] ") + C("The plain text for the given hash is: ") + R(pwd))
				break
	else:
		wl = open(f"{wordlist}", "r", encoding='latin-1')
		for pwd in wl:
			a = pwd.encode()
			b = a[:-1]
			md5 = hashlib.md5(b).hexdigest()
			if hashed == md5:
				print(G("\n[!] ") + C("Cracked the given hash: ") + R(hashed) + G("\n[!] ") + C("The plain text for the given hash is: ") + R(pwd))
				break

##### Main function
def main():
	
	string = f"{args.hash}"
	hashed = f"{args.bruteforce}"
	wordlist = f"{args.wordlist}"
	file = f"{args.file}"
	output = f"{args.output}"

	if string != "None" and hashed == "None" and wordlist == "None" and file == "None" and output == "None":
		hash()
	elif string == "None" and hashed != "None" and wordlist == "None" and file == "None" and output == "None":
		bf()
	elif string == "None" and hashed != "None" and wordlist != "None" and file == "None" and output == "None":
		bf()
	elif string == "None" and hashed == "None" and wordlist == "None" and file != "None" and output == "None":
		hash_file()
	elif string == "None" and hashed == "None" and wordlist == "None" and file != "None" and output != "None":
		hash_file()
	else:
		error(R("I don't know what to do\n"))

if __name__ == '__main__':
	main()
