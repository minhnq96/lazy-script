#!/bin/bash

## Color code variables
red="\e[0;91m"
yellow="\e[0;33m"
blue="\e[0;94m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
green="\e[0;92m"
white="\e[0;97m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"
date=$(date)

## BANNER

echo -e "${blue}${bold}			:::..::::::::...:........:...:........:::::${reset}"
echo -e "${blue}${bold}			:::'########:::::'########::::'########::::${reset}"
echo -e "${blue}${bold}			::::##.... ##:::::##.....::::::##.....:::::${reset}"
echo -e "${blue}${bold}			::::##:::: ##:::::##:::::::::::##::::::::::${reset}"
echo -e "${blue}${bold}			::::########::::::######:::::::######::::::${reset}"
echo -e "${blue}${bold}			::::##.....:::::::##...::::::::##...:::::::${reset}"
echo -e "${blue}${bold}			::::##:::::::'###:##::::::'###:##::::::::::${reset}"
echo -e "${blue}${bold}			::::##::::::::###:########:###:########::::${reset}"
echo -e "${blue}${bold}			:::..::::::::...:........:...:........:::::${reset}"

#echo -e "${red}${bold}A Post Exploit Enum Script${reset}"

print_support (){
  printf """
	${green}/---------------------------------------------------------------------------\\
	|                         ${blue}A Post Exploit Enum Script${green}                        |
	|---------------------------------------------------------------------------| 
	|                   ${yellow}Author${green}       :     ${red}Minh Nguyen Quang${green}                    |
	|                   ${yellow}Version${green}      :     ${red}1.0${green}                                  |
	|                   ${yellow}Created${green}      :     ${red}6/12/2021${green}                            |
	|---------------------------------------------------------------------------|
	|          ${blue}The script was ran at: ${yellow}${date}${green}           |
	\---------------------------------------------------------------------------/
"""
}

print_support

# Get user password if possible
user=$(whoami)
echo -e "\n${green}${bold}Give me user ${yellow}${bold}$user${green}${bold}'s password${red}${bold} [If you don't know leave it blank]${green}${bold}: "
read password

## Begin enum process
printf "${red}${bold}\nPERFORMING THE ENUMERATION PROCESS\n"

## Check to see the current user
echo -e "${green}${bold}\n[+] The user you are currently logged in is: ${reset}"
echo -e "whoami: ${yellow}$(whoami)${reset}"
echo -e "id: ${yellow}$(id)${reset}\n"

# echo -e "${red}Remember to manually check ${blue}${bold}'sudo -l'${red} to see what your current user can run as sudo on the machine${reset}"

echo -e "${green}${bold}\n[+] List user's privileges using ${red}${bold}sudo -l${reset}"

sudo -nv > /dev/null;

if 	[ "$?" -ne 0 ]
then
	echo -e "\n${yellow}${bold}$user ${red}${bold}required a password to run sudo${reset}"
	if [ -z "$password" ]
	then
	      echo -e "\n${red}${bold}Could not check! You don't know the password for user ${yellow}${bold}$user${reset}"
	else
	      echo -e "\n${red}$(echo $password | sudo -S -l)${reset}"
	fi
else
	echo -e "\n${red}$(sudo -l)${reset}"
fi

## Check the current PATH
echo -e "${green}${bold}\n[+] The current PATH: ${reset}"
echo -e "${yellow}$(echo $PATH)${reset}"

## Check the current version of sudo
echo -e "${green}${bold}\n[+] The current version of sudo: ${reset}"
echo -e "${yellow}$(sudo --version)${reset}"

## Check the kernel of the machine
echo -e "${green}${bold}\n[+] Checking for the kernel of the machine: ${reset}"
echo -e "${yellow}$(uname -a)${reset}"
echo -e "\n${yellow}$(cat /etc/os-release)${reset}"

echo -e "${green}${bold}\n[+] Some more additional informations: ${reset}"
echo -e "${yellow}$(lscpu | grep "Hypervisor vendor")${reset}"
echo -e "${yellow}$(lscpu | grep Architecture)${reset}"
echo -e "${yellow}$(lscpu | grep Byte)${reset}"

## Check for useful binaries
echo -e "${green}${bold}\n[+] Useful binaries exist on the machine: ${reset}"
binaries='nmap aws nc ncat netcat nc.traditional wget curl ping gcc g++ make gdb base64 socat python python2 python3 python2.7 python2.6 python3.6 python3.7 perl php ruby xterm doas sudo fetch docker lxc ctr runc rkt kubectl'
for tool in $binaries; do command -v "$tool";done

## Check the active port of the machine
echo -e "${green}${bold}\n[+] Checking for the active port of the machine: ${reset}"
echo -e "${yellow}$(netstat -tlunw)${reset}"

## Check for permission on /etc/shadow and /etc/passwd
echo -e "${green}${bold}\n[+] Checking for permission on /etc/shadow: ${reset}"
echo -e "$(ls -la /etc/shadow)"
echo -e "${green}${bold}\n[+] Checking for permission on /etc/passwd:${reset}"
a=$(ls -l /etc/passwd | cut -d' ' -f1)
per=$(stat --printf '%a' /etc/passwd)
owner=$(ls -l /etc/passwd | cut -d' ' -f3)
group=$(ls -l /etc/passwd | cut -d' ' -f4)
passwd=$(ls -l /etc/passwd | cut -d' ' -f10)

other_per="${a:7:9}"
group_per="${a:4:3}"
owner_per="${a:1:3}"

if [ ${owner} == "root" ]
then
	owner="${red}${bold}${owner}${reset}"
else
	owner="${blue}${bold}${owner}${reset}"
fi

if [ ${group} == "root" ]
then
	group="${red}${bold}${group}${reset}"
else
	group="${blue}${bold}${group}${reset}"
fi

echo -e "$(ls -l /etc/passwd)\n"
echo -e "Current ${green}${bold}/etc/passwd${reset} permission is: ${red}${bold}${per}${reset}"
echo -e "File owner: ${owner}"
echo -e "File group: ${group}"
echo -e "Owner Permissions: ${red}${bold}${owner_per}${reset}"
echo -e "Group Permissions: ${red}${bold}${group_per}${reset}"
echo -e "Others Permissions: ${red}${bold}${other_per}${reset}"

## Display users on the machine
echo -e "${green}${bold}\n[+] Display user on the machine base on /etc/passwd: ${reset}"

echo -e "${yellow}----------[ Users with console ]---------------${reset}"
echo -e "${blue}$(cat /etc/passwd | grep "sh$")${reset}"

echo -e "${yellow}\n----------[ Superusers ]---------------${reset}"
echo -e "${red}$(awk -F: '($3 == "0") {print}' /etc/passwd)${reset}"

## Looking for id_rsa keys
echo -e "${green}${bold}\n[+] Looking for id_rsa ${reset}"
### Set up the var
home=$(ls /home)
echo -e "$home" > .home.txt
a=$(pwd)/.home.txt
### Look for the id_rsa
while read line; do
	if [ -f /home/$line/.ssh/id_rsa ]
	then
		echo -e "${green}${bold}[+] Found ssh key for user ${yellow}${bold}$line: ${reset}"
		echo -e "${yellow}${bold}\ncat /home/$line/.ssh/id_rsa${reset}"
		echo -e "\n==================== ${yellow}${bold}$line${red}${bold}'s id_rsa${reset} ====================\n${reset}"
		echo -e "${red}$(cat /home/$line/.ssh/id_rsa)${reset}"
	else
		echo -e "${green}${bold}[+] Did not find ssh key for user $line${reset}"
	fi
done < $a
### Clean up
if [ -f $a ]
then
	rm -rf $a
fi

## List every unique user has login session
echo -e "${green}${bold}\n[+] Checking current login session with unique users: ${reset}"
echo -e "${uline}Current session login: ${reset}"
echo -e "${yellow}$(w)${reset}"
echo -e "\n${uline}Unique users: ${reset}"
echo -e "${yellow}$(who | cut -d " " -f 1 | sort -u)${reset}"

## List of users running a process
echo -e "${green}${bold}\n[+] List of users are currently running a process: ${reset}"
echo -e "${red}$(ps -eaho user | sort -u)${reset}"

## Check for any available cronjob
echo -e "${green}${bold}\n[+] Checking for cronjobs: ${reset}"
echo -e "${yellow}$(cat /etc/crontab)${reset}"

## Check for cap files
echo -e "${green}${bold}\n[+] Checking for capabilities files: ${reset}"
echo -e "${yellow}$(getcap -r / 2>/dev/null)${reset}"

## Check for SUID files
echo -e "${green}${bold}\n[+] Looking for all SUID files: ${reset}"
echo -e "${yellow}$(find / -perm -4000 -type f 2>/dev/null | grep bin | sort -u)${reset}"
echo -e "\n"

## Listing vulnerable SUID found
# https://linuxhint.com/bash_loop_list_strings/
declare -a list=("ab" "agetty" "alpine" "ansible-playbook" "apt-get" "apt" "ar" "aria2c" "arj" "arp" "as" "ascii-xfr" "ascii85" "ash" "aspell" "at" "atobm" "awk" "base32" "base58" "base64" "basenc" "basez" "bash" "bpftrace" "bridge" "bundler" "busctl" "busybox" "byebug" "bzip2" "c89" "c99" "cancel" "capsh" "cat" "cdist" "certbot" "check_by_ssh" "check_cups" "check_log" "check_memory" "check_raid" "check_ssl_cert" "check_statusfile" "chmod" "choom" "chown" "chroot" "cmp" "cobc" "column" "comm" "composer" "cowsay" "cowthink" "cp" "cpan" "cpio" "cpulimit" "crash" "crontab" "csh" "csplit" "csvtool" "cupsfilter" "curl" "cut" "dash" "date" "dd" "dialog" "diff" "dig" "dmesg" "dmidecode" "dmsetup" "dnf" "docker" "dosbox" "dpkg" "dvips" "easy_install" "eb" "ed" "efax" "emacs" "env" "eqn" "ex" "exiftool" "expand" "expect" "facter" "file" "find" "finger" "fish" "flock" "fmt" "fold" "fping" "ftp" "gawk" "gcc" "gcore" "gdb" "gem" "genie" "genisoimage" "ghc" "ghci" "gimp" "ginsh" "git" "grc" "grep" "gtester" "gzip" "hd" "head" "hexdump" "highlight" "hping3" "iconv" "iftop" "install" "ionice" "ip" "irb" "ispell" "jjs" "join" "journalctl" "jq" "jrunscript" "jtag" "knife" "ksh" "ksshell" "kubectl" "latex" "latexmk" "ld.so" "ldconfig" "less" "lftp" "ln" "loginctl" "logsave" "look" "lp" "ltrace" "lua" "lualatex" "luatex" "lwp-download" "lwp-request" "mail" "make" "man" "mawk" "more" "mosquitto" "mount" "msgattrib" "msgcat" "msgconv" "msgfilter" "msgmerge" "msguniq" "mtr" "multitime" "mv" "mysql" "nano" "nasm" "nawk" "nc" "neofetch" "nice" "nl" "nm" "nmap" "node" "nohup" "npm" "nroff" "nsenter" "octave" "od" "openssl" "openvpn" "openvt" "opkg" "paste" "pax" "pdb" "pdflatex" "pdftex" "perf" "perl" "pg" "php" "pic" "pico" "pidstat" "pip" "pkexec" "pkg" "pr" "pry" "psftp" "psql" "ptx" "puppet" "python" "rake" "readelf" "red" "redcarpet" "restic" "rev" "rlogin" "rlwrap" "rpm" "rpmdb" "rpmquery" "rpmverify" "rsync" "ruby" "run-mailcap" "run-parts" "rview" "rvim" "sash" "scanmem" "scp" "screen" "script" "scrot" "sed" "service" "setarch" "setfacl" "sftp" "sg" "shuf" "slsh" "smbclient" "snap" "socat" "soelim" "sort" "split" "sqlite3" "ss" "ssh-keygen" "ssh-keyscan" "ssh" "sshpass" "start-stop-daemon" "stdbuf" "strace" "strings" "su" "sysctl" "systemctl" "systemd-resolve" "tac" "tail" "tar" "task" "taskset" "tasksh" "tbl" "tclsh" "tcpdump" "tee" "telnet" "tex" "tftp" "tic" "time" "timedatectl" "timeout" "tmate" "tmux" "top" "troff" "tshark" "ul" "unexpand" "uniq" "unshare" "unzip" "update-alternatives" "uudecode" "uuencode" "valgrind" "vi" "view" "vigr" "vim" "vimdiff" "vipw" "virsh" "volatility" "wall" "watch" "wc" "wget" "whiptail" "whois" "wireshark" "wish" "xargs" "xdotool" "xelatex" "xetex" "xmodmap" "xmore" "xpad" "xxd" "xz" "yarn" "yash" "yelp" "yum" "zathura" "zip" "zsh" "zsoelim" "zypper" "python3.10" "python3.9" "python3.8" "python3.7" "python3.6" "python3.5" "python3.4" "python3.3" "python3.2" "python3.1" "python3")

a=()
b=$(echo -e "$(find / -perm -4000 -type f 2>/dev/null | grep bin | sort -u)")

for i in $b; do
	a+=($i)
done

for x in "${list[@]}"; do
	for y in "${a[@]}"; do
		if [ "$x" = "${y##*/}" ]; then
			binary+=("$y")
		fi
	done
done

# For if else help "https://linuxize.com/post/bash-if-else-statement/"
if [ -z $binary ]; then
	echo -e "${blue}${bold}Found no vulnerable SUID${reset}"
else
	for x in "${binary[@]}"; do
		if [[ $x =~ .*"python3".* ]]; then
			echo -e "${red}${bold}Found vulnerable SUID: ${green}${bold}$x ${red}${bold}(URL: https://gtfobins.github.io/gtfobins/python/)${reset}"
		else
			echo -e "${red}${bold}Found vulnerable SUID: ${green}${bold}$x ${red}${bold}(URL: https://gtfobins.github.io/gtfobins/${x##*/}/)${reset}"
		fi
	done
fi

## Check for files with open permissions
echo -e "${green}${bold}\n[+] Looking for all files with open permissions: ${reset}"
echo -e "${yellow}$(find / -perm -777 -type f 2>/dev/null)${reset}"

## Check for files with writable permission for current user or current group
echo -e "${green}${bold}\n[+] Looking for files with writable permission for current user or current group: ${reset}"
echo -e "${yellow}\n----------[ For current user ]----------${reset}"
echo -e "${yellow}$(find / -perm /u+w -type f -user `whoami` 2>/dev/null | grep -Ewv '(mnt|boot|run|srv|dev|media|proc|sys|home|lib|libx32|usr|bin|etc|lib64|lib32|sbin)')${reset}"
echo -e "${yellow}\n----------[ For current group ]----------${reset}"
echo -e "${yellow}$(find / -perm /g+w -type f 2>/dev/null | grep -Ewv '(mnt|boot|run|srv|dev|media|proc|sys|home|lib|libx32|usr|bin|etc|lib64|lib32|sbin)')${reset}"

## Find directories with writable permissions for current user or current group
echo -e "${green}${bold}\n[+] Looking for directories with writable permission for current user or current group: ${reset}"
echo -e "${yellow}$(find / -perm /u+w,g+w -d -type d -user `whoami` 2>/dev/null | grep -Ewv '(mnt|boot|run|srv|dev|media|proc|sys|home|lib|libx32|usr|bin|etc|lib64|lib32|sbin)')${reset}"
