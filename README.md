# These are all of my lazy scripts that i made.

> Minh Nguyen Quang | 12-03-2021

----------------------------------------------

# Script that you should use

- **Please only use the script that listed here, because of the other used for my CTF labs, so if you run those scripts it will messed up your ENV.**

**1. install-docker-kali.sh**

- For install `docker` and `docker-compose` on the kali image 2020.4/2021.1.

**2. install-docker-ubuntu.sh**

- For install `docker` and `docker-compose` on the ubuntu.

- Work on ubuntu 18.04, 20.01 and 20.04. **Older version of Ubuntu had not been tested**.

**3. kubectl-install-kali.sh**

- For install `kubectl` on kali with latest version of itself.

- Tested on Ubuntu 20.01 - working fine.

**4. terraform-install-ubuntu.sh**

- For install `terraform` on ubuntu.

- Tested on Ubuntu 20.04.2.0.

**5. sublime-install.sh**

- For install `sublime-text` on ubuntu/debian.

**6. pwncat-install.sh**

- For install `pwncat` on ubuntu/debian (work in progress)

**7. rot13_decode.py**

- To decrypt `rot13` string right away, instead going online and find a online decryption website

**8. md5.py**

- Use when you want to hash a string with `md5` or want to crack a `md5` string to plain text using wordlist `rockyou.txt`

**9. url.py**

- Use when you want to `url encode` or `url decode` a string

**10. binary.py**

- Use when you want to `binary encode or decode` a string

# Script that you should not use

- **Please DO NOT USE the script that listed here, because i only used them for my CTF labs, so if you run these scripts it will messed up your ENV.**

**1. dns-setup.sh**

- For installing a dns server on Ubuntu that hosting the CTFd.

**2. ftp-installscript.sh**

- For quickly install ftp with anonymous access.

**3. ssh-install.sh**

- For install ssh when connect to a SCS machine.

- Need to use with `after-banner` and `before-banner`.
